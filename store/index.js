const store = {
  state: () => ({
    league: {},
    chosenTeams: [],
    chosenPlayer: null
  }),
  mutations: {
    setLeague(state, league) {
      state.league = league
    },
    addTeam(state, team) {
      //If there are less than three teams
      //we push the new one into the array
      if (state.chosenTeams.length < 3) {
        state.chosenTeams.push(team)
      }
      //Otherwise we do nothing, there should probably 
      //be some rejection handler here
    },
    removeTeam(state, team) {
      let index = state.chosenTeams.findIndex(chosenTeam => chosenTeam.teamId === team.teamId)
      if (index !== -1) {
        state.chosenTeams[index].chosenIndex = null
        state.chosenTeams.splice(index, 1)
      }
    },
    choosePlayer(state, player) {
      state.chosenPlayer = player
    }
  }
}

export default store