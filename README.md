# forza

> Test assignment for Forza Football

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## What should be done
* Cleaning the localStorage at the final stage, so the user can bet again without clicking everything away.
* An easier way of reordering teams, probably making the entries in the summary card draggable