//this file is responsible for keeping the Vuex state between page reloads

import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR, app }) => {
  if (isHMR) return

  if (!app.mixins) {
    app.mixins = []
  }

  app.mixins.push({
    mounted() {
      createPersistedState({})(store)
    }
  })
}